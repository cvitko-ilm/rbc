param
(
    # The datasource string for the DB system alias of environment
    [Parameter(Mandatory)]
    [ValidateNotNullOrEmpty()]
    [String] $DataSource,

    # The database name to use
    [Parameter(Mandatory)]
    [ValidateNotNullOrEmpty()]
    [String] $DatabaseName,

    # Path to the SQL file to send
    [Parameter(Mandatory)]
    [ValidateNotNullOrEmpty()]
    [String] $SqlFilePath,

    # Path to the SQL file to send
    [Parameter(Mandatory)]
    [ValidateNotNullOrEmpty()]
    [String] $SqlFileName,

    # Path to the SQL file to send
    [Parameter(Mandatory)]
    [ValidateNotNullOrEmpty()]
    [String] $DBApiBaseUrl
    
)

# Call API to send SQL file

$urlSendAPI = $DBApiBaseUrl+"/upload"

$filePathAndFile = $SqlFilePath+$SqlFileName

if (-Not (Test-Path $filePathAndFile -PathType Leaf)){
    Write-Host "No SQL file found, skipping SQL deployment."
    exit
}

$fileBin = [IO.File]::ReadAllBytes($filePathAndFile)

# Convert byte-array to string (without changing anything)
#
$enc = [System.Text.Encoding]::GetEncoding("iso-8859-1")
$fileEnc = $enc.GetString($fileBin)

<#
# PowerShell does not (yet) have built-in support for making 'multipart' (i.e. binary file upload compatible)
# form uploads. So we have to craft one...
#
# This is doing similar to: 
# $ curl -i -F "file=@file.any" -F "computer=MYPC" http://url
#
# Boundary is anything that is guaranteed not to exist in the sent data (i.e. string long enough)
#    
# Note: The protocol is very precise about getting the number of line feeds correct (both CRLF or LF work).
#>
$boundary = [System.Guid]::NewGuid().ToString()

$LF = "`r`n"
$bodyLines = (
    "--$boundary",
    "Content-Disposition: form-data; name=`"file`"; filename=`"201802281331_testfunction.sql`"",
    "Content-Type: application/octet-stream$LF",
    $fileEnc,
    "--$boundary",
    "Content-Disposition: form-data; name=`"Description`"$LF",
    $env:BUILD_BUILDNumber,
    "--$boundary",
    "Content-Disposition: form-data; name=`"DataSource`"$LF",
    $DataSource,
    "--$boundary",
    "Content-Disposition: form-data; name=`"DatabaseName`"$LF",
    $DatabaseName,
    "--$boundary--$LF"
    ) -join $LF

try {
    # Returns the response gotten from the server (we pass it on).
    #
    $response = Invoke-RestMethod -Uri $urlSendAPI -Method Post -ContentType "multipart/form-data; boundary=`"$boundary`"" -Body $bodyLines
    Write-Verbose "---- Response from API - Send File -----"
    Write-Verbose $response

    $ExecutionId = $response.ExecutionID
    Write-Verbose "ExecutionId: $ExecutionId"

    #Check the status until success or fail
    $urlStatusApi = $DBApiBaseUrl+"/status/$ExecutionId/false"
    $responseStatus = "In Progress"
    while($responseStatus -eq "In Progress"){
        $response = Invoke-RestMethod -Uri $urlStatusApi -Method Get -ContentType "application/json"
        Write-Verbose "---- Response from API - Status -----"
        Write-Verbose $response
        $responseStatus = $response.Status
    }

    If($response.Status -eq "Failed"){
        #Get error scripts to output
        $urlErrorApi = $DBApiBaseUrl+"/errors/$ExecutionId/false"
        $response = Invoke-RestMethod -Uri $urlErrorApi -Method Get -ContentType "application/json"
        Write-Verbose "---- Response from API - Errors -----"
        Write-Verbose $response
        foreach($errorMessage in $response.ErrorMessages)
        {
            Write-Verbose $errorMessage
            $errorMsg = $errorMessage.Message
            $scriptCode = $errorMessage.ScriptCode
            Write-Error "Message: $errorMsg `n Script Code: $scriptCode"
        }
    }
}
catch [System.Net.WebException] {
    Write-Error( "FAILED to reach '$URL': $_" )
    throw $_
}